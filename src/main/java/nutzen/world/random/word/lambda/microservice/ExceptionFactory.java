package nutzen.world.random.word.lambda.microservice;

/**
 * See https://aws.amazon.com/blogs/compute/error-handling-patterns-in-amazon-api-gateway-and-aws-lambda/
 *
 * @author joeels
 */
public final class ExceptionFactory {

    private ExceptionFactory() {
    }
    
    public static RuntimeException badRequest(final String message) {
        
        return new BadRequestException("[BadRequest] " + message);
    }
    
    public static RuntimeException internalServerError(final String message) {
        
        return new InternalServerError("[InternalServerError] " + message);
    }
}
