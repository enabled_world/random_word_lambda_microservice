package nutzen.world.random.word.lambda.microservice;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 * RandomWordLambdaMicroservice
 * nutzen.world.random.word.lambda.microservice.RamdomWordRequestHandler::handleRequest
 * 
 * @author joeels
 */
public class RamdomWordRequestHandler implements RequestHandler<RequestClass, String> {

    private static final String DELIMITER = ".";
    private static final int NOT_SPECIFIED = -1;
    private static final int MAXIMUM_LENGTH = 255;
    private static final int MAXIMUM_NUMBER_OF_WORDS = 10;
    private static final int MINIMUM_LENGTH = 1;
    private static final int MINIMUM_NUMBER_OF_WORDS = 1;
    private static final Logger LOGGER = Logger.getLogger(RamdomWordRequestHandler.class);
    private static final List<String> MASTER_WORD_LIST;

    static {
        final Pattern pattern = Pattern.compile("^[\\p{Alnum}]+$");
        final Set<String> words = new HashSet<>();

        // sourced from https://github.com/dwyl/english-words
        try (final InputStream inputStream = RamdomWordRequestHandler.class.getResourceAsStream("/words.txt")) {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            for (String word = reader.readLine(); (null != word); word = reader.readLine()) {

                final Matcher matcher = pattern.matcher(word);
                if (matcher.matches()) {
                    words.add(word);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Unexpected IOException", e);
        }

        MASTER_WORD_LIST = Collections.unmodifiableList(new ArrayList<>(words));
    }

    @Override
    public String handleRequest(final RequestClass input, final Context context) {

        final String delimiter = stringValue(input.getDelimiter(), DELIMITER);
        final int length = intValue(input.getLength(), NOT_SPECIFIED);
        final int maximumLength = intValue(input.getMaximumLength(), MAXIMUM_LENGTH);
        final int minimumLength = intValue(input.getMinimumLength(), MINIMUM_LENGTH);
        final int numberOfWords = intValue(input.getNumberOfWords(), MINIMUM_NUMBER_OF_WORDS);

        LOGGER.debug(String.format("delimiter='%s' length=%d maximum_length=%d minimum_length=%d number_of_words=%d", delimiter, length, maximumLength, minimumLength, numberOfWords));

        if (NOT_SPECIFIED != length) {
            validate("length", length, MINIMUM_LENGTH, MAXIMUM_LENGTH);
        }
        validate("maximumLength", maximumLength, MINIMUM_LENGTH, MAXIMUM_LENGTH);
        validate("minimumLength", minimumLength, MINIMUM_LENGTH, maximumLength);
        validate("numberOfWords", numberOfWords, MINIMUM_NUMBER_OF_WORDS, MAXIMUM_NUMBER_OF_WORDS);

        final List<String> wordList = loadWordList(length, maximumLength, minimumLength);
        LOGGER.debug(String.format("Loaded %d words.", wordList.size()));
        validateWordList(wordList, length, maximumLength, minimumLength, numberOfWords);

        final String word = randomWord(wordList, delimiter, numberOfWords);
        LOGGER.debug(String.format("random_word='%s'", word));

        return word;
    }

    private static int intValue(final Integer value, final int defaultValue) {

        return (null == value) ? defaultValue : value;
    }

    private static String stringValue(final String value, final String defaultValue) {

        return (null == value) ? defaultValue : value;
    }

    private static void validate(final String name, final int value, final int minimumValue, final int maximumValue) {

        if ((minimumValue > value) || (maximumValue < value)) {
            throw ExceptionFactory.badRequest(String.format("Parameter '%s' is %d, but it must be in range [%d, %d].", name, value, minimumValue, maximumValue));
        }
    }

    private static void validateWordList(
            final List<String> wordList,
            final int length,
            final int maximumLength,
            final int minimumLength,
            final int numberOfWords) {

        if (numberOfWords > wordList.size()) {
            final String message;

            if (NOT_SPECIFIED == length) {
                message = String.format("Not enough words with lengths in range [%d, %d].", minimumLength, maximumLength);
            } else {
                message = String.format("Not enough words with length of %d.", length);
            }

            throw ExceptionFactory.badRequest(message);
        }
    }

    private static List<String> loadWordList(
            final int length,
            final int maximumLength,
            final int minimumLength) {

        if ((null == MASTER_WORD_LIST) || MASTER_WORD_LIST.isEmpty()) {
            throw ExceptionFactory.internalServerError("Empty master word list!");
        }

        final List<String> wordList = new ArrayList<>(MASTER_WORD_LIST.size());

        MASTER_WORD_LIST.stream().filter((word) -> (!filtered(word, length, maximumLength, minimumLength))).forEach((word) -> {
            wordList.add(word);
        });

        return wordList;
    }

    private static boolean filtered(
            final String word,
            final int length,
            final int maximumLength,
            final int minimumLength) {

        final boolean filtered;

        if (NOT_SPECIFIED == length) {
            if (maximumLength < word.length()) {
                filtered = true;
            } else if (minimumLength > word.length()) {
                filtered = true;
            } else {
                filtered = false;
            }
        } else {
            filtered = (length != word.length());
        }

        return filtered;
    }

    private static String randomWord(
            final List<String> wordList,
            final String delimiter,
            final int numberOfWords) {

        final StringBuilder stringBuilder = new StringBuilder();

        final Random random = new Random();
        for (int counter = 0; numberOfWords > counter; ++counter) {
            final int index = random.nextInt(wordList.size());
            final String word = wordList.get(index);
            wordList.remove(index);

            if (0 < counter) {
                stringBuilder.append(delimiter);
            }
            stringBuilder.append(word);
        }

        return stringBuilder.toString();
    }
}
