package nutzen.world.random.word.lambda.microservice;

import java.io.Serializable;

/**
 *
 * @author joeels
 */
public class RequestClass implements Serializable {

    private String delimiter;
    private Integer length;
    private Integer maximumLength;
    private Integer minimumLength;
    private Integer numberOfWords;

    /**
     * @return the delimiter
     */
    public String getDelimiter() {
        return delimiter;
    }

    /**
     * @param delimiter the delimiter to set
     */
    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    /**
     * @return the length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return the maximumLength
     */
    public Integer getMaximumLength() {
        return maximumLength;
    }

    /**
     * @param maximumLength the maximumLength to set
     */
    public void setMaximumLength(Integer maximumLength) {
        this.maximumLength = maximumLength;
    }

    /**
     * @return the minimumLength
     */
    public Integer getMinimumLength() {
        return minimumLength;
    }

    /**
     * @param minimumLength the minimumLength to set
     */
    public void setMinimumLength(Integer minimumLength) {
        this.minimumLength = minimumLength;
    }

    /**
     * @return the numberOfWords
     */
    public Integer getNumberOfWords() {
        return numberOfWords;
    }

    /**
     * @param numberOfWords the numberOfWords to set
     */
    public void setNumberOfWords(Integer numberOfWords) {
        this.numberOfWords = numberOfWords;
    }

}
