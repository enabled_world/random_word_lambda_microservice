package nutzen.world.random.word.lambda.microservice;

import org.apache.log4j.Logger;

/**
 *
 * @author joeels
 */
public final class RuntimeHelper {

    private static final Logger LOGGER = Logger.getLogger(RuntimeHelper.class);

    private RuntimeHelper() {
    }

    public static void logMemory() {

        final Runtime runtime = Runtime.getRuntime();
        final double freeMemory = toMegaBytes(runtime.freeMemory());
        final double maxMemory = toMegaBytes(runtime.maxMemory());
        final double totalMemory = toMegaBytes(runtime.totalMemory());

        LOGGER.debug(String.format("free_memory=%.2fMB max_memory=%.2fMB total_memory=%.2fMB", freeMemory, maxMemory, totalMemory));
    }

    private static double toMegaBytes(final long bytes) {

        double megaBytes = bytes;
        megaBytes /= (1024 * 1024);

        return megaBytes;
    }

}
