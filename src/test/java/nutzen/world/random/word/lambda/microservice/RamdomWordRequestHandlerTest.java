package nutzen.world.random.word.lambda.microservice;

import com.amazonaws.services.lambda.runtime.Context;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author joeels
 */
public class RamdomWordRequestHandlerTest {

    private static final Logger LOGGER = Logger.getLogger(RamdomWordRequestHandlerTest.class);

    @Test
    public void simple() {

        final RamdomWordRequestHandler handler = new RamdomWordRequestHandler();
        final RequestClass input = new RequestClass();
        final Context context = null;

        RuntimeHelper.logMemory();
        LOGGER.debug("random_word=" + handler.handleRequest(input, context));
        RuntimeHelper.logMemory();
        System.gc();
        RuntimeHelper.logMemory();
    }

    @Test
    public void simple2() {

        final RamdomWordRequestHandler handler = new RamdomWordRequestHandler();
        final RequestClass input = new RequestClass();
        input.setMinimumLength(3);
        input.setMaximumLength(8);
        input.setNumberOfWords(2);
        final Context context = null;

        LOGGER.debug("random_word=" + handler.handleRequest(input, context));
    }

    @Test(expected = BadRequestException.class)
    public void simpleBadRequest0() {

        final RamdomWordRequestHandler handler = new RamdomWordRequestHandler();
        final RequestClass input = new RequestClass();
        input.setMinimumLength(10);
        input.setMaximumLength(8);
        final Context context = null;

        LOGGER.debug("random_word=" + handler.handleRequest(input, context));
        Assert.fail("This should not happen!");
    }

    @Test(expected = BadRequestException.class)
    public void simpleBadRequest1() {

        final RamdomWordRequestHandler handler = new RamdomWordRequestHandler();
        final RequestClass input = new RequestClass();
        input.setMinimumLength(255);
        final Context context = null;

        LOGGER.debug("random_word=" + handler.handleRequest(input, context));
        Assert.fail("This should not happen!");
    }

    @Test(expected = BadRequestException.class)
    public void simpleBadRequest2() {

        final RamdomWordRequestHandler handler = new RamdomWordRequestHandler();
        final RequestClass input = new RequestClass();
        input.setLength(255);
        final Context context = null;

        LOGGER.debug("random_word=" + handler.handleRequest(input, context));
        Assert.fail("This should not happen!");
    }
}
